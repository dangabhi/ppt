Inductive myTrue : Prop := truthIsObvious. (* Truth has one constructor *)
Inductive myFalse : Prop:=.

Set Implicit Arguments.
Definition neg (A : Prop) := A -> myFalse.

Print myTrue.
Print True.
Print myTrue_ind.
Print myTrue_rect.


Definition anotherProof : myTrue
  := truthIsObvious.

(* Using tactics *)
Lemma proofOfTruth : myTrue.
Proof.
  exact truthIsObvious.
Qed.


Definition yetAnotherProof : myTrue.
Proof.
  exact truthIsObvious.
Qed.

Inductive And (A B : Prop) : Prop :=
| mkPair : A -> B -> And A B.

Inductive Or (A B : Prop) : Prop :=
| inl : A -> Or A B
| inr : B -> Or A B
.

Print inl.
Print mkPair.


Definition x: And myTrue myTrue := mkPair truthIsObvious truthIsObvious.

Definition demorgan1 (A B : Prop): neg (Or A B) ->  And (neg A) (neg B).
Proof.
  intro f.
  compute.
  apply mkPair. Show Proof. intro a. apply f. apply inl. exact a.
  intro b. apply f. apply inr. exact b.
  Show Proof.
Qed.  
