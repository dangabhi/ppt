(* This is the typed version of the calculator. From Chapter 2 of cpdt *)

Require Import Nat.
Require Import Bool.
Require Import List.

Inductive type : Set := Nat | Bool.


Definition typeDenote (t : type) :=
  match t with
    | Nat  => nat
    | Bool => bool
  end.


Inductive tbinop : type -> type -> type -> Set :=
| tPlus            : tbinop Nat Nat Nat
| tMul             : tbinop Nat Nat Nat
| tLe              : tbinop Nat Nat Bool
| tEq   {t : type} : tbinop t   t   Bool
.

(*
Inductive texpr : type -> Set :=
| tNConst : nat  -> texpr Nat
| tBConst : bool -> texpr Bool
| tBinOp  {t1 t2 t3 : type} : (tbinop t1 t2 t3) -> texpr t1 -> texpr t2 -> texpr t3
.
 *)

Inductive texpr : type -> Set :=
| tConst {t : type} : typeDenote t  -> texpr t
| tBinOp  {t1 t2 t3 : type} : (tbinop t1 t2 t3) -> texpr t1 -> texpr t2 -> texpr t3
.

(* Now comes the stack machine. *)

(* The type of the stack *)
Definition tstack := list type.

(* The value stack *)
Fixpoint vstack (ts : tstack) : Set :=
  match ts with
    | nil       => unit
    | t :: ts   => typeDenote t * vstack ts
  end.


Inductive tinstr : tstack  -> tstack -> Set :=
| tpush {ts : tstack}{t : type}     : typeDenote t -> tinstr  ts (t :: ts)
| texec {ts : tstack}{s t r : type} : tbinop s t r -> tinstr (s :: t :: ts) (r :: ts)
.

Inductive tprogram : tstack -> tstack -> Set :=
| halt  {s : tstack}        : tprogram s s
| doit  {s1 s2 s3 : tstack} : tinstr s1 s2 -> tprogram s2 s3  -> tprogram s1 s3
.

Fixpoint app_p {s t} (p : tprogram s t) : forall {tp}, tprogram t tp -> tprogram s tp :=
  match p with
    | halt      => fun _ p => p
    | doit i pp => fun _ p => doit i (app_p pp p)
  end.

Notation "{{ x }}"  := (doit x halt).
Notation "x +++ y"  := (app_p x y) (at level 80, right associativity).



(* meaning of the constructs of the language *)

Definition tbinopDenote {s t r : type}(op : tbinop s t r)
: typeDenote s -> typeDenote t -> typeDenote r
  :=
  match op in tbinop s t r return  typeDenote s -> typeDenote t -> typeDenote r
  with
    | tPlus    => plus
    | tMul     => mult
    | tLe      => Nat.leb
    | @tEq Nat => Nat.eqb
    | @tEq Bool => eqb
  end.


Fixpoint texprDenote {t : type}(e : texpr t) : typeDenote t :=
  match e with
    | tConst  c          => c
    | tBinOp  op e1 e2   => tbinopDenote op (texprDenote e1) (texprDenote e2)
  end.








Definition tinstrDenote {s0 s1} (i : tinstr s0 s1)  :=
  match i in tinstr s0 s1 return vstack s0 -> vstack s1
  with
    | tpush  x  => fun vs  => (x, vs)
    | texec op
      => fun vs => let '(a,(b,vsp)) := vs in (tbinopDenote op a b , vsp)
  end.

Fixpoint tprogramDenote {s0 s1}(p : tprogram s0 s1) :=
  match p with
    | halt       => fun vs => vs
    | doit i pp  => fun vs => tprogramDenote pp (tinstrDenote i vs)
  end.


(* meanings *)



(* The compiler *)
(*
Fixpoint compile {t}{ts}(e : texpr t) :  tprogram ts (t :: ts)  :=
  match e in texpr t return tprogram ts (t :: ts) with
    | tConst c         => {{ tpush c }}
    | tBinOp op e1 e2  => ({{ texec op }} +++ (compile e2)) +++ (compile e1)
  end.

*)